# Arbeitsabläufe

- Bücher Anlegen			(oft)		(Fall: Buchspende/Nachkauf)
- Exemplar hinzufügen (Automatisch)	(oft)		(Fall: Buchspende/Nachkauf)
- Exemplar hinzufügen (Manuell)		(seltener)	(Fall: Buchspende/Nachkauf)
- Etiketten drucken			(oft)		(Fall: Neu Codieren/Ausleihkarte)
- Bestandsliste				(seltener)	(Fall: Nachfrage von Lehrern)
- Bücher aus dem System nehmen		(selten)	(Fall: Aussortieren)
- Schüler hinzufügen			(selten)	(Fall: Neue Schüler)
- Schüler entfernen			(selten)	(Fall: Schulabgänger)
- Automatische Liste von OT		(jährlich 1x)	(Fall: Neues Schuljahr)
