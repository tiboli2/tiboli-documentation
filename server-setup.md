# Server-setup

Full featured guide on how to setup a server for TiBoLi.

Overview:

1. [Install operating system](#install-operating-system)
2. [Install Docker](#install-docker-and-docker-compose)
3. [Install the TiBoLi-Boundle](#install-the-tiboli-server-boundle)
4. [(optional) Setup mDNS](#setup-mdns)
5. [Start the software](#start-the-software)

## Install Operating System

### Choose your version

I am using `Ubuntu-Server 22.04.1 LTS` and this tutorial is specified on this version. On other versions it may differ from this.

### Install OS

I won't go into details on how to install Ubuntu on a server. in short:

1. Make a Bootstick
2. Boot from Bootstick
3. Follow installation guide provided by the OS.

## Install Docker and Docker-Compose

Just follow [this tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04) for detail but I will give all the commands down below.

```bash
sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt install docker-ce docker-compose
```

Once it is installed check if it is running via

```bash
sudo systemctl status docker
```

To use the docker commands without root (if you wan't that), add your user to the `docker` group, which is created during the installation. and then log out and back in.

```bash
sudo usermod -aG docker ${USER}
su - ${USER}
```

Now you can validate that docker is running fine by running the [hello-world](https://hub.docker.com/_/hello-world)docker container.

```bash
docker run hello-world
```

## Install the TiBoLi-Server Boundle

Clone the [TiBoLi-Docker](https://gitlab.com/tiboli2/tiboli-docker) repository:

```bash
git clone git@gitlab.com:tiboli2/tiboli-docker.git
git clone https://gitlab.com/tiboli2/tiboli-docker.git
```

And create a `.env` file with the following content:

```env
ACCESS_TOKEN_SECRET=[YOUR 32-BYTE-HEX-SECRET]
REFRESH_TOKEN_SECRET=[OTHER-32-BYTE-SECRET]

DATABASE_NAME=[eg. tiboli]
DATABASE_PASSWORD=[PASSWORDS NORMALY KEPT SECRET]

SERVER_PORT=3005
HOST_NAME=[Your Domain / Hostname / IP-Adress]
PROTOCOL=http
```

To generate the Access and refresh token I use the node cli and the `randomBytes()` function from the build in `crypto` package:

```javascript
require("crypro").randomBytes(32).toString("hex");
```

It doesn't matter if you use a Hex string, base64 would also work fine. It also doesn't matter how long the secret string is. Longer = More security because it is harder to brute-force. If you don't want do use node or dont have it installed, use a generator of your choice. I can recoment [this one](https://seanwasere.com/generate-random-hex/).

## Setup mDNS

To access the Software locally by a domain and not its IP-Adress, we use the mDNS protocol.

The Multicast DNS (mDNS) protocol is a way for devices on a local network to discover each other and communicate without the need for a centralized Domain Name System (DNS) server. It uses the same programming interfaces, packet formats, and operating semantics as the traditional unicast DNS, but instead of sending queries to a centralized DNS server, mDNS-enabled devices multicast their queries to all devices on the network using the reserved IP multicast address.

Install a mDNS server with the following command:

```bash
sudo apt-get install avahi-daemon
```

and check if it is running with `systemctl status acahi-daemon`. The Server is now available under the `[HOSTNAME].local` Domain.

To make the Software available on this domain set the `HOST_NAME` setting in `.env`-File to your `.local` domain.

## Start the Software

To now run the app, just use the `docker-compose.yml` file with:

```bash
docker-compose --env-file .env up -d
```

The Docker Container are configured to always restart. To Stop them use:

```bash
docker-compose down
```

When first running the app, the Database Container might take some time to initialize itself so that the defined Healthcheck for the api container will fail, resulting in it not starting. If you encounter that error, please let the containers running without the api and try to restart the compose a few minutes later by using the same command. It will then say that the db, fe and traefik are already running but the api will be started then. If the database is initialized once, the healthcheck should not fail on startup anymore so that all servers are started as intended.

## Update the Software

To update the software, first stop it using `docker-compose down`. Then pull the new images either by directly pulling them using `docker pull` or pull all of them with `docker-compose pull`. After that just restart it as described in [Start the software](#start-the-software). 