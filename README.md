# TiBoLi-Documentation

Welcome to the documentation of the software 'TiBoLi'! With this application, you can easily and effectively manage the books and customers in your library. 'TiBoLi', which stands for 'Tiny Book Library', is a modern software solution that is set up on a server with [Docker](https://www.docker.com/). The application is built using [NodeJS](https://nodejs.org/) as the server backend and [Angular](https://angular.io/) as the frontend. It can be easily accessed and used through a website.

'TiBoLi' was primarily developed for schools and school libraries and offers functions specifically tailored to their needs. By using 'TiBoLi', schools and school libraries can easily and efficiently manage their book collections. The application is user-friendly and offers an intuitive interface that is easy to use even for inexperienced users.

With 'TiBoLi', you can manage the books in your library by capturing information such as title, author, publisher, and publication year. Additionally, you can store information about your customers, such as name, address, and ID number. With the application's simple user interface, you can quickly and effectively manage your library and always keep track of your collections.

This documentation provides you with a detailed overview of all the features of 'TiBoLi' and shows you how to use the application to manage your library effectively. Moreover, the documentation also explains how to add functions to the software to make it fit your individual needs.

## Index

- Installation guide

## Installation

The software is operated with Docker and consists of two application parts and a database. The server backend is a [NodeJS](https://nodejs.org/) application that uses [Express](https://expressjs.com/) to provide an API and communicates with the MySQL database. The frontend is an [Angular](https://angular.io/) application that delivers a single-page application for the web.

To install it on a Server, follow [this guide](server-setup.md)
